package ir.smartpass.enums;

import lombok.Getter;

@Getter
public enum MediaType implements IConvertible<String> {
    ALBUM("ALBUM"),
    BOOK("BOOK");

    private String code;


    MediaType(String code) {
        this.code = code;
    }

    public static MediaType findByTitle(String title) {
        if (title == null) {
            return null;
        }
        for (MediaType resultCode : MediaType.values()) {
            if (resultCode.getCode().equals(title)) {
                return resultCode;
            }
        }
        return null;
    }

    @Override
    public String getValue() {
        return code;
    }
}
