package ir.smartpass.services.interfaces;

import ir.smartpass.dto.external.googleBook.response.BookResult;

public interface IBookService {
    BookResult search(String searchTerm);
}
