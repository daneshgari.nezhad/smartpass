package ir.smartpass.services.impl;

import ir.smartpass.dto.external.googleBook.response.BookResult;
import ir.smartpass.repository.BookCacheRepository;
import ir.smartpass.services.client.google.GoogleBookService;
import ir.smartpass.services.interfaces.IBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BookServiceImpl implements IBookService {
    private final GoogleBookService bookService;
    private final BookCacheRepository bookCacheRepository;

    public BookServiceImpl(GoogleBookService bookService, BookCacheRepository bookCacheRepository) {
        this.bookService = bookService;
        this.bookCacheRepository = bookCacheRepository;
    }

    public BookResult search(String searchTerm) {
        log.info("service:BookServiceImpl,method:search, searchTerm : " + searchTerm);
        BookResult bookResults = bookCacheRepository.get(searchTerm);
        if (bookResults == null) {
            bookResults = bookService.searchBooks(searchTerm);
            log.info("service:BookServiceImpl,method:save, searchTerm : " + searchTerm,"desc: save on cache");
            bookCacheRepository.save(searchTerm, bookResults);
        }
        return bookResults;
    }
}
