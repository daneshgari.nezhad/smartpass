package ir.smartpass.services.impl;

import ir.smartpass.dto.external.itunes.response.MusicResult;
import ir.smartpass.repository.MusicCacheRepository;
import ir.smartpass.services.client.itunes.AlbumService;
import ir.smartpass.services.interfaces.ITunesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TunesServiceImpl implements ITunesService {
    private final AlbumService albumService;
    private final MusicCacheRepository musicCacheRepository;

    public TunesServiceImpl(AlbumService albumService, MusicCacheRepository musicCacheRepository) {
        this.albumService = albumService;
        this.musicCacheRepository = musicCacheRepository;
    }


    public MusicResult search(String searchTerm) {
        log.info("service:TunesServiceImpl,method:save, searchTerm : " + searchTerm);
        MusicResult musicResult = musicCacheRepository.get(searchTerm);
        if (musicResult == null) {
            musicResult = albumService.searchAlbums(searchTerm);
            log.info("service:TunesServiceImpl,method:save, searchTerm : " + searchTerm, "desc: save on cache");
            musicCacheRepository.save(searchTerm, musicResult);
        }
        return musicResult;
    }
}
