package ir.smartpass.dto.external.googleBook.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReadingMode {
    private Boolean text;
    private Boolean image;
}
