package ir.smartpass.dto.external.googleBook.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VolumeInfo {
    private String title;
    private String subtitle;
    private List<String> authors = new ArrayList<>();
    private String publisher;
    private String publishedDate;
    private String description;
    private List<IndustryIdentifier> industryIdentifiers = new ArrayList<>();
    private ReadingMode readingModes;
    private Integer pageCount;
    private String printType;
    private List<String> categories = new ArrayList<>();
    private String maturityRating;
    private Boolean allowAnonLogging;
    private String contentVersion;
    private PanelizationSummary panelizationSummary;
    private ImageLink imageLinks;
    private String language;
    private String previewLink;
    private String infoLink;
    private String canonicalVolumeLink;
}
