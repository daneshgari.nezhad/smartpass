package ir.smartpass.dto.external.itunes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MusicSearchRequest {
    private String term;
    private String country;
    private String entity;
    private Integer limit;
}
